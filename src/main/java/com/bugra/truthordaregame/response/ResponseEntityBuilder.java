package com.bugra.truthordaregame.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseEntityBuilder {

    public static ResponseEntity createResponse(boolean result){

        ResponseEntity responseEntity = new ResponseEntity("SORUNUZ EKLENEMEDİ ÇÜNKÜ GİRDİGİNİZ LEVEL MEVCUT DEGİLDİR, LÜTFEN GEÇERLİ BİR LEVEL GİRİN (1-2-3)", HttpStatus.NOT_FOUND);

        if (result)
             responseEntity = new ResponseEntity("SORUNUZ EKLENDİ", HttpStatus.CREATED);

        return responseEntity;
    }

    public static ResponseEntity isQuestionExistControl(boolean result){
        ResponseEntity responseEntity = new ResponseEntity("DB'DE BÖYLE BİR SORU YOKTUR", HttpStatus.NOT_FOUND);

        if (result)
            return responseEntity = new ResponseEntity("ARATTIGINIZ SORU DB'DE VARDIR", HttpStatus.OK);

        return responseEntity;
    }


    public static ResponseEntity updateQuestion(boolean result){
        ResponseEntity responseEntity = new ResponseEntity("GÜNCELLEME İŞLEMİ BAŞARISIZ OLMUŞTUR", HttpStatus.NOT_FOUND);

        if (result)
             responseEntity = new ResponseEntity("GÜNCELLEME İŞLEMİ BAŞARILI", HttpStatus.OK);

        return responseEntity;
    }

    public static ResponseEntity deleteQuestion(boolean result){
        ResponseEntity responseEntity = new ResponseEntity("SİLME İŞLEMİ BAŞARISIZ OLMUŞTUR", HttpStatus.NOT_FOUND);

        if (result)
            return responseEntity = new ResponseEntity("SİLME İŞLEMİ BAŞARILI", HttpStatus.ACCEPTED);

        return responseEntity;
    }


}
