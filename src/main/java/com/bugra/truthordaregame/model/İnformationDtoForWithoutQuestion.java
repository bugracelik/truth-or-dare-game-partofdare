package com.bugra.truthordaregame.model;

public class İnformationDtoForWithoutQuestion {
    private String message;

    public İnformationDtoForWithoutQuestion() {
    }

    public İnformationDtoForWithoutQuestion(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
