package com.bugra.truthordaregame.request;


public class UpdateQuestionByIdRequest {
    private Integer id;
    private String question;
    private Integer level;

    public UpdateQuestionByIdRequest() {
    }

    public UpdateQuestionByIdRequest(Integer id, String question, Integer level) {
        this.id = id;
        this.question = question;
        this.level = level;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "UpdateRequest{" +
                "id=" + id +
                ", question='" + question + '\'' +
                ", level=" + level +
                '}';
    }
}
