package com.bugra.truthordaregame.controller;

import com.bugra.truthordaregame.request.CreateQuestionRequest;
import com.bugra.truthordaregame.request.DeleteQuestionByIdRequest;
import com.bugra.truthordaregame.request.UpdateQuestionByIdRequest;
import com.bugra.truthordaregame.response.ResponseEntityBuilder;
import com.bugra.truthordaregame.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@Component
@RestController
public class GameRestController {

    @Autowired
    GameService gameService;

    @GetMapping("isQuestionExistControl/{id}/{level}")
    public ResponseEntity isQuestionExistControl(@PathVariable Integer id, @PathVariable Integer level) {
        boolean questionExistControl = gameService.isQuestionExistControl(id, level);
        return ResponseEntityBuilder.isQuestionExistControl(questionExistControl);
    }

    //Use POST APIs to create new subordinate resources
    //Create resources
    @PostMapping("add/question")
    public ResponseEntity addQuestion(@RequestBody CreateQuestionRequest createRequest) {
        boolean result = gameService.addQuestion(createRequest);
        return ResponseEntityBuilder.createResponse(result);
    }

    //Use GET requests to retrieve resource representation/information only – and not to modify it in any way
    //Read resources
    @GetMapping("random/{level}")
    public List getQuestionForLevel(@PathVariable Integer level) {
        return gameService.getQuestionForLevel(level);
    }

    //Use PUT APIs primarily to update existing resource
    //Update resources
    @PutMapping("update")
    public ResponseEntity updateQuestion(@RequestBody UpdateQuestionByIdRequest updateRequest) throws SQLException {
        boolean result = gameService.updateQuestion(updateRequest);
        return ResponseEntityBuilder.updateQuestion(result);

    }

    //As the name applies, DELETE APIs are used to delete resources
    //Delete resources
    @DeleteMapping("delete")
    public ResponseEntity deleteQuestion(@RequestBody DeleteQuestionByIdRequest deleteRequst) {
        boolean result = gameService.deleteQuestion(deleteRequst);
        return ResponseEntityBuilder.deleteQuestion(result);
    }
}
