package com.bugra.truthordaregame.rowmapper;


import com.bugra.truthordaregame.domain.Question;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class QuestionRowMapper implements RowMapper {
    @Override
    public Question mapRow(ResultSet resultSet, int i) throws SQLException {

        Integer id = resultSet.getInt("id");
        String questiondb = resultSet.getString("question");
        Integer level = resultSet.getInt("level");

        Question question = new Question(id, questiondb, level);
        return question;

    }
}
